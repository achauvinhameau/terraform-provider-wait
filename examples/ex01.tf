provider "wait" {
}

resource "wait_sleep" "ex01" {
  delay = 1000
}

resource "wait_sleep" "ex02" {
  delay = 5000
}

output "ex01" {
 value = "id:${wait_sleep.ex01.id} version:${wait_sleep.ex01.version} sleep:${wait_sleep.ex01.delay}ms"
}
output "ex02" {
 value = "id:${wait_sleep.ex02.id} version:${wait_sleep.ex02.version} sleep:${wait_sleep.ex02.delay}ms"
}