// +build all sleep
// to test only these features: -tags sleep -run="XX"

package wait

import (
	"fmt"
	"log"
	"testing"

	"github.com/hashicorp/terraform/helper/resource"
)

func TestAccSleep_01(t *testing.T) {
	resource.Test(t, resource.TestCase{
		PreCheck: func() {
			testAccPreCheck(t)
		},
		Providers: testProviders,
		Steps: []resource.TestStep{
			{
				Config: Config_TestAccSleep_01("test", 10),
				Check: resource.ComposeTestCheckFunc(
					resource.TestCheckResourceAttrSet("wait_sleep.test", "id"),
				),
			},
		},
	})
}

func Config_TestAccSleep_01(name string, delay int) string {
	return fmt.Sprintf(`
    resource "wait_sleep" "%s" {
       delay = %d
    }
`, name, delay)
}

func TestAccSleep_02(t *testing.T) {
	resource.Test(t, resource.TestCase{
		PreCheck: func() {
			testAccPreCheck(t)
		},
		Providers: testProviders,
		Steps: []resource.TestStep{
			{
				Config: Config_TestAccSleep_02(),
				Check: resource.ComposeTestCheckFunc(
					resource.TestCheckResourceAttrSet("wait_sleep.test1", "id"),
					resource.TestCheckResourceAttrSet("wait_sleep.test2", "id"),
				),
			},
		},
	})
}

func Config_TestAccSleep_02() string {
	log.Printf("[DEBUG] Config_TestAccSleep_02")

	return fmt.Sprintf(`
    resource "wait_sleep" "test1" {
       delay = 10
    }

    resource "wait_sleep" "test2" {
       delay = 1000
    }
`)
}

func TestAccSleep_03(t *testing.T) {
	resource.Test(t, resource.TestCase{
		PreCheck: func() {
			testAccPreCheck(t)
		},
		Providers: testProviders,
		Steps: []resource.TestStep{
			{
				Config: Config_TestAccSleep_01("test", 10),
				Check: resource.ComposeTestCheckFunc(
					resource.TestCheckResourceAttrSet("wait_sleep.test", "id"),
					resource.TestCheckResourceAttr("wait_sleep.test", "delay", "10"),
				),
			},
			{
				Config: Config_TestAccSleep_01("test", 20),
				Check: resource.ComposeTestCheckFunc(
					resource.TestCheckResourceAttrSet("wait_sleep.test", "id"),
					resource.TestCheckResourceAttr("wait_sleep.test", "delay", "20"),
				),
			},
		},
	})
}
