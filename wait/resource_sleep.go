package wait

import (
	"github.com/hashicorp/terraform/helper/schema"
	"log"
	"time"
	// "strconv"
)

func resourcesleep() *schema.Resource {
	return &schema.Resource{
		Create: resourcesleepCreate,
		Read:   resourcesleepRead,
		Update: resourcesleepUpdate,
		Delete: resourcesleepDelete,

		Schema: map[string]*schema.Schema{
			"delay": &schema.Schema{
				Type:        schema.TypeInt,
				Description: "ms to sleep",
				Required:    true,
				ForceNew:    false,
			},
			"version": &schema.Schema{
				Type:        schema.TypeString,
				Description: "ms to sleep",
				Computed:    true,
				ForceNew:    false,
				Default:     nil,
			},
		},
	}
}

func resourcesleepCreate(d *schema.ResourceData, m interface{}) error {
	log.Printf("[INFO] ** create")
	s := m.(*Wait)
	d.SetId(getNewWaitID())
	d.Set("version", s.Version)

	if delay, ok := d.Get("delay").(int); ok {
		log.Printf("[DEBUG] sleep %d ms", delay)
		time.Sleep(time.Duration(delay) * time.Millisecond)
	}

	return resourcesleepRead(d, m)
}

func resourcesleepRead(d *schema.ResourceData, m interface{}) error {
	log.Printf("[INFO] ** read")
	return nil
}

func resourcesleepUpdate(d *schema.ResourceData, m interface{}) error {
	log.Printf("[INFO] ** update")

	if delay, ok := d.Get("delay").(int); ok {
		log.Printf("[DEBUG] updated %d ms", delay)
		time.Sleep(time.Duration(delay) * time.Millisecond)
	}

	return resourcesleepRead(d, m)
}

func resourcesleepDelete(d *schema.ResourceData, m interface{}) error {
	log.Printf("[INFO] ** delete")

	d.SetId("")

	return nil
}
