package wait

import (
	"log"
	"strconv"
)

// Wait structure definition
type Wait struct {
	Version string
}

var id = 0
var sVersion = "1.0"

// NewWait creates a Wait object, for now with only the version
// not used to store any information
func NewWait() *Wait {
	log.Printf("[DEBUG] NewWait")

	s := &Wait{}
	s.Version = sVersion

	return s
}

// getNewWaitId provides a new ID for each object created
func getNewWaitID() string {
	id = id + 1
	log.Printf("[DEBUG] getNewWaitId now %d", id)

	return strconv.Itoa(id)
}
