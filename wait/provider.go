package wait

import (
	"github.com/hashicorp/terraform/helper/schema"
	"github.com/hashicorp/terraform/terraform"

	"log"
)

// Provider is the main function of the wait module for terraform
//  maps wait_sleep function
func Provider() terraform.ResourceProvider {
	return &schema.Provider{
		Schema: map[string]*schema.Schema{},

		DataSourcesMap: map[string]*schema.Resource{},

		ResourcesMap: map[string]*schema.Resource{
			"wait_sleep": resourcesleep(),
		},

		ConfigureFunc: ProviderConfigure,
	}
}

// ProviderConfigure is only here for starting with a good template
func ProviderConfigure(d *schema.ResourceData) (interface{}, error) {
	log.Printf("[DEBUG] configure")
	s := NewWait()

	return s, nil
}
