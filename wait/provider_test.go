package wait

import (
	"log"
	"testing"

	"github.com/hashicorp/terraform/helper/schema"
	"github.com/hashicorp/terraform/terraform"
)

var testProviders map[string]terraform.ResourceProvider
var testProvider *schema.Provider

func testAccPreCheck(t *testing.T) {
	log.Printf("[DEBUG] - testPreCheck\n")
}

func init() {
	testProvider = Provider().(*schema.Provider)
	testProviders = map[string]terraform.ResourceProvider{
		"wait": testProvider,
	}
}
