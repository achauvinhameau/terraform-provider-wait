# terraform-provider-wait

simple terraform provider to wait during execution of a plan

# Example

```
provider "wait" {
}

resource "wait_sleep" "ex01" {
  delay = 1000
}
```

At apply, the wait_sleep.ex01 will wait for 1 second. At update of the delay value, the new delay will be apply in a sleep.

# Build from sources

```
git clone https://gitlab.com/achauvinhameau/terraform-provider-wait.git
cd terraform-provider-wait.git
go get
go build
```

Depending on your OS, move the binary `terraform-provider-wait[.exe]` towards `%APPDATA%\terraform.d\plugins\windows_amd64\` (Windows 10) or `$HOME/.terraform.d/plugins/` (Linux) for example. For more examples, please refer to the terraform documentation.

