package main

import (
	"github.com/hashicorp/terraform/plugin"
	"gitlab.com/achauvinhameau/terraform-provider-wait/wait"
)

func main() {
	plugin.Serve(&plugin.ServeOpts{
		ProviderFunc: wait.Provider,
	})
}
